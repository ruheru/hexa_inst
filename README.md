Apps Hexa Institute

Database : PostgreSql
Java Version : 8
SpringBoot Version : 2

have 8 tables relation
- Siswa
- Jurusan
- Sesi
- Ruang_kelas
- Mata_kuliah
- Rencana_Studi
- Pertemuan
- Pertemuan_dtl

Cara running : 
1. Buat database local di postgresql dgn nama : db_gredu
2. Running dengan command : mvn clean spring-boot:run

Access Swagger Documentation API : 
http://localhost:8080/swagger-ui.html