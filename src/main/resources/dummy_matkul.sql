delete from public.mata_kuliah;

INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (1,now(), now(), 'mt1', 'matkul1', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (2,now(), now(), 'mt2', 'matkul2', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (3,now(), now(), 'mt3', 'matkul3', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (4,now(), now(), 'mt4', 'matkul4', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (5,now(), now(), 'mt5', 'matkul5', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (6,now(), now(), 'mt6', 'matkul6', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (7,now(), now(), 'mt7', 'matkul7', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (8,now(), now(), 'mt8', 'matkul8', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (9,now(), now(), 'mt9', 'matkul9', 1);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (10,now(), now(), 'mt10', 'matkul10', 1);

INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (11,now(), now(), 'mt11', 'matkul11', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (12,now(), now(), 'mt12', 'matkul12', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (13,now(), now(), 'mt13', 'matkul13', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (14,now(), now(), 'mt14', 'matkul14', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (15,now(), now(), 'mt15', 'matkul15', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (16,now(), now(), 'mt16', 'matkul16', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (17,now(), now(), 'mt17', 'matkul17', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (18,now(), now(), 'mt18', 'matkul18', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (19,now(), now(), 'mt19', 'matkul19', 2);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (110,now(), now(), 'mt110', 'matkul110', 2);

INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (21,now(), now(), 'mt21', 'matkul21', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (22,now(), now(), 'mt22', 'matkul22', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (23,now(), now(), 'mt23', 'matkul23', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (24,now(), now(), 'mt24', 'matkul24', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (25,now(), now(), 'mt25', 'matkul25', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (26,now(), now(), 'mt26', 'matkul26', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (27,now(), now(), 'mt27', 'matkul27', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (28,now(), now(), 'mt28', 'matkul28', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (29,now(), now(), 'mt29', 'matkul29', 3);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (210,now(), now(), 'mt210', 'matkul210', 3);

INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (41,now(), now(), 'mt41', 'matkul41', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (42,now(), now(), 'mt42', 'matkul42', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (43,now(), now(), 'mt43', 'matkul43', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (44,now(), now(), 'mt44', 'matkul44', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (45,now(), now(), 'mt45', 'matkul45', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (46,now(), now(), 'mt46', 'matkul46', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (47,now(), now(), 'mt47', 'matkul47', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (48,now(), now(), 'mt48', 'matkul48', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (49,now(), now(), 'mt49', 'matkul49', 4);
INSERT INTO public.mata_kuliah(id, created_at, updated_at, code, name, id_jurusan)VALUES (410,now(), now(), 'mt410', 'matkul410', 4);