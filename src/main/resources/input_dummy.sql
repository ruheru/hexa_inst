SELECT FROM public.ruang_kelas;

INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (1, now(), now(), 'r1', 'Ruang 1');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (2, now(), now(), 'r2', 'Ruang 2');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (3, now(), now(), 'r3', 'Ruang 3');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (4, now(), now(), 'r4', 'Ruang 4');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (5, now(), now(), 'r5', 'Ruang 5');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (6, now(), now(), 'r6', 'Ruang 6');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (7, now(), now(), 'r7', 'Ruang 7');
INSERT INTO public.ruang_kelas(id, created_at, updated_at, code, description) VALUES (8, now(), now(), 'r8', 'Ruang 8');
