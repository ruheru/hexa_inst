DELETE FROM public.jurusan;

INSERT INTO public.jurusan(id, created_at, updated_at, code, name) VALUES (1, now(), now(), 'tech', 'Technology');
INSERT INTO public.jurusan(id, created_at, updated_at, code, name) VALUES (2, now(), now(), 'eco', 'Economy');
INSERT INTO public.jurusan(id, created_at, updated_at, code, name) VALUES (3, now(), now(), 'mng', 'Management');
INSERT INTO public.jurusan(id, created_at, updated_at, code, name) VALUES (4, now(), now(), 'health', 'Health');