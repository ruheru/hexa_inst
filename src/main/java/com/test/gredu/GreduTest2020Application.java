package com.test.gredu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreduTest2020Application {

	public static void main(String[] args) {
		SpringApplication.run(GreduTest2020Application.class, args);
	}

}
