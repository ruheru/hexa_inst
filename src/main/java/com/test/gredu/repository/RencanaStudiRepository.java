package com.test.gredu.repository;

import com.test.gredu.model.RencanaStudi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RencanaStudiRepository extends JpaRepository<RencanaStudi, Long> {
}
