package com.test.gredu.repository;

import com.test.gredu.model.MataKuliah;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MataKuliahRepository extends JpaRepository<MataKuliah,Long> {
}
