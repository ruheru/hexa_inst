package com.test.gredu.repository;

import com.test.gredu.model.PertemuanDtl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PertemuanDtlRepository extends JpaRepository<PertemuanDtl,Long> {
}
