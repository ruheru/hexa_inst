package com.test.gredu.repository;

import com.test.gredu.model.RuangKelas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuangKelasRepository extends JpaRepository<RuangKelas,Long> {
}
