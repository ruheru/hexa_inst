package com.test.gredu.repository;

import com.test.gredu.model.Sesi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SesiRepository extends JpaRepository<Sesi,Long> {
}
