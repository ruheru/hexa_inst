package com.test.gredu.repository;

import com.test.gredu.model.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiswaRepository extends JpaRepository<Siswa,Long> {
}
