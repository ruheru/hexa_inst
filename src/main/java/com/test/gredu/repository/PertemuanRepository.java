package com.test.gredu.repository;

import com.test.gredu.model.Pertemuan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PertemuanRepository extends JpaRepository<Pertemuan,Long> {
}
