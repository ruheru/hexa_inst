package com.test.gredu.repository;

import com.test.gredu.model.Jurusan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JurusanRepository extends JpaRepository<Jurusan,Long> {
}
