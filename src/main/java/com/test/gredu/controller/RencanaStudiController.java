package com.test.gredu.controller;

import com.test.gredu.model.RencanaStudi;
import com.test.gredu.service.RencanaStudiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RequestMapping("/api/rencanastudi")
@RestController
public class RencanaStudiController {
    @Autowired
    RencanaStudiService rencanaStudiService;

    @GetMapping("/findall")
    public Object findAll(){
        return rencanaStudiService.findAll();
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody RencanaStudi rencanaStudi){
        rencanaStudi.setCreatedAt(new Date());
        rencanaStudi.setUpdatedAt(new Date());
        return rencanaStudiService.save(rencanaStudi);
    }
}
