package com.test.gredu.controller;

import com.test.gredu.model.Siswa;
import com.test.gredu.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/siswa")
@RestController
public class SiswaController {
    @Autowired
    SiswaService siswaService;

    @GetMapping("/findall")
    public Object findAll(){
        return siswaService.findAll();
    }

    @GetMapping("/findById/{id}")
    public Object findById(@PathVariable("id") Long id){
        return siswaService.findById(id);
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Siswa siswa){
        return siswaService.save(siswa);
    }

    @PostMapping("/edit/{id}")
    public ResponseEntity<?> update(@RequestBody Siswa siswa, @PathVariable("id") Long id){
        return siswaService.edit(siswa,id);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody Siswa siswa){
        return siswaService.delete(siswa);
    }
}
