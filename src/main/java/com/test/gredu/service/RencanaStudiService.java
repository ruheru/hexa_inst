package com.test.gredu.service;

import com.test.gredu.model.RencanaStudi;
import com.test.gredu.repository.RencanaStudiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RencanaStudiService {
    @Autowired
    RencanaStudiRepository rencanaStudiRepository;

    
    public Object findAll() {
        return rencanaStudiRepository.findAll();
    }

    public ResponseEntity<?> save(RencanaStudi rencanaStudi) {
        rencanaStudiRepository.save(rencanaStudi);
        return new ResponseEntity<>(rencanaStudi, HttpStatus.CREATED);
    }
}
