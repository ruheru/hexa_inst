package com.test.gredu.service;

import com.test.gredu.model.Siswa;
import com.test.gredu.repository.SiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SiswaService {
    @Autowired
    SiswaRepository siswaRepository;

    public Object findAll() {
        return siswaRepository.findAll();
    }

    public Object findById(Long id){
        return siswaRepository.findById(id);
    }

    public ResponseEntity<?> save(Siswa siswa){
        siswa.setCreatedAt(new Date());
        siswa.setUpdatedAt(new Date());
        siswaRepository.save(siswa);
        return new ResponseEntity<>(siswa, HttpStatus.CREATED);
    }

    public ResponseEntity<?> edit(Siswa siswa, Long id){
        siswa.setId(id);
        siswaRepository.save(siswa);
        return new ResponseEntity<>(siswa, HttpStatus.OK);
    }

    public ResponseEntity<?> delete(Siswa siswa){
        siswaRepository.delete(siswa);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
