package com.test.gredu.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Siswa extends CreatedUpdateAt{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String nik;

    @Temporal(TemporalType.DATE)
    private Date dob;

    private String phoneNumber;
    private String email;
    private String gender;

}
